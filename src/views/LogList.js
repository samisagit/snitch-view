import React, {Component} from 'react'
import FilterInput from "../components/FilterInput"

class LogList extends Component {
	constructor() {
		super()
		this.state = {
			logs: [],
			filters: [
				{
					name: "environment",
					options: [
						"production",
						"staging",
						"local",
					],
				},
				{
					name: "severity",
					options: [
						"info",
						"error",
						"fatal",
					],
				},
			],
			selectedFilters: {
				severity: "",
				environment: "",
			}
		}
	}

	updateLogs = () => {
		let url = "http://0.0.0.0:81/logs/?hack"
		let envs = this.state.selectedFilters.environment
		let sevs = this.state.selectedFilters.severity
		url = envs ? url + "&environment=" + envs : url
		url = sevs ? url + "&severity=" + sevs : url
		fetch(
			url,
			{
				headers: new Headers({
					"Authorization": "eyJhbGciOiAiSFMyNTYiLCJ0eXAiOiAiSldUIn0=.eyJhY2NvdW50SWQiOiAxLCJlbnZpcm9ubWVudCI6ICIifQ==.594ca85bebe805f86c8684eebfe35a8a26efdecf02bb393c1e8c0e14ac914799",
				})
			}
		)
			.then(results => {
				return results.json()
			})
			.then(data => {
				this.setState({logs: data})
			})
	}

	onFilterChange = (e) => {
		let key = e.target.name
		let val = e.target.value
		let innerState = {...this.state.selectedFilters}
		innerState[key] = val
		this.setState({selectedFilters: innerState})
	}

	render() {
		return (
			<div>
				<h1>Logs</h1>
				{this.state.filters.map((filter, i) => (
					<FilterInput key={i} options={filter.options} name={filter.name} onChange={this.onFilterChange}/>
				))}
				<button onClick={this.updateLogs}>Go</button>
				<ol>
					{this.state.logs.map((item, i) => (
						<li key={i}>
							<h4>{item.Severity}: {item.Message}</h4>
							<p>Environment: {item.Environment}</p>
							<pre>{item.Stack}</pre>
						</li>
					))}
				</ol>
			</div>
		)
	}

}

export default LogList

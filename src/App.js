import React, {Component} from 'react'
import LogList from "./views/LogList"

class App extends Component {
	render() {
		return (
			<div className="App">
				<LogList />
			</div>
		)
	}
}

export default App

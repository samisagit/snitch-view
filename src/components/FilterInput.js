import React, {Component} from 'react'

class FilterInput extends Component {
	constructor() {
		super()
	}

	handleChange = (e) => {
		this.props.onChange(e)
	}

	render() {
		return (
			<select name={this.props.name} type="select" onChange={this.handleChange}>
				<option selected="selected">Select {this.props.name}</option>
				{this.props.options.map((item, i) => (
					<option key={i}>{item}</option>
				))}
			</select>
		)
	}

}

export default FilterInput
